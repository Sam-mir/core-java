package Inheritance;

public class TestManager {

	public static void main(String[] args) {
		Manager boss = new Manager("Huzaifa", 70000, 2010, 11, 21);
		boss.setBonus(2000);
		
		Employee[] staff = new Employee[3];
		
		staff[0] = boss;
		staff[1] = new Employee("Mamoon", 30000, 2010, 2, 12);
		staff[2] = new Employee("Snobar", 2000, 2010, 3, 11);
		
		for(Employee e : staff) {
			System.out.println("Name= " + e.getName() + 
					           "Salary= " + e.getSalary());
		}
		
		

	}

}
