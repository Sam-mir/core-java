package inheritance.abstractClasses;



public class PersonTest {

	public static void main(String[] args) {
		Person[] people = new Person[2];
		
		people[0] = new Employee("Rameez", 30000, 2011, 2, 12);
		people[1] = new Student("Imtiaz", "Computer Sciences");
		
		for(Person p : people) {
			System.out.println(p.getName() + "," + p.getDescription());
		}

	}

}
