package basics.classes;

import java.time.LocalDate;
import java.util.GregorianCalendar;
import java.util.Calendar;

public class DemoClass {
	public static void main(String args[]) {

		System.out.println(LocalDate.now());

		LocalDate newYearsEve = LocalDate.of(1999, 12, 31);
		System.out.println(newYearsEve);

		int year = newYearsEve.getYear();
		System.out.println(year);

		int month = newYearsEve.getMonthValue(); // 12
		System.out.println(month);

		int day = newYearsEve.getDayOfMonth(); // 31
		System.out.println(day);

		// plusDays doesnt mutate newYearsEve its an acessor method
		LocalDate aThousandDaysLater = newYearsEve.plusDays(1000);
		System.out.println(aThousandDaysLater);

		year = aThousandDaysLater.getYear(); // 2002
		System.out.println(year);

		month = aThousandDaysLater.getMonthValue(); // 09
		System.out.println(month);

		day = aThousandDaysLater.getDayOfMonth(); // 26
		System.out.println(day);
		// mutator method add changes someDay values
		GregorianCalendar someDay = new GregorianCalendar(1999, 11, 31);
		// Odd feature of that class: month numbers go from 0 to 11
		someDay.add(Calendar.DAY_OF_MONTH, 1000);

		year = someDay.get(Calendar.YEAR); // 2002
		month = someDay.get(Calendar.MONTH) + 1; // 09
		day = someDay.get(Calendar.DAY_OF_MONTH); // 26

		System.out.println("year " + year + " month " + month + " day " + day);
		// acessor method get of GregorianCalendar
		year = someDay.get(Calendar.YEAR); // 2002
		month = someDay.get(Calendar.MONTH) + 1; // 09
		day = someDay.get(Calendar.DAY_OF_MONTH); // 26
	}

}
